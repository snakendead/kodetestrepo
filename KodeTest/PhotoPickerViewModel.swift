//
//  PhotoPickerViewModel.swift
//  KodeTest
//
//  Created by developer2 on 28.07.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

class PhotoPickerViewModel: BaseViewModel {

    let media: [MediaEntity]
    
    required init(viewModelService: ViewModelServiceProtocol, media: [MediaEntity]) {
        self.media = media
        super.init(viewModelService: viewModelService)
    }
    
}
