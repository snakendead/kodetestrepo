//
//  FindUserViewController.swift
//  KodeTest
//
//  Created by developer2 on 28.07.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit	
import ReactiveCocoa

class FindUserViewController: BaseViewController, UIWebViewDelegate {

    var viewModel: FindUserViewModel
    
    init(viewModel: FindUserViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "FindUserViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBOutlet weak var findButton: UIButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var goButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var findButtonBottomConstraint: NSLayoutConstraint!
    
    override func firstInitialize() {        
        navigationController?.navigationBar.tintColor = UIColor.MyColorGreen()
        
        backView.layer.cornerRadius = 2.0
        backView.layer.masksToBounds = false
        
        let shadowLayer = CALayer()
        shadowLayer.cornerRadius = 2.0
        shadowLayer.backgroundColor = UIColor.whiteColor().CGColor
        shadowLayer.shadowColor = UIColor.blackColor().CGColor
        shadowLayer.shadowRadius = 1
        shadowLayer.shadowOpacity = 0.24
        shadowLayer.shadowOffset = CGSizeMake(0, 1.0)
        shadowLayer.shouldRasterize = true
        RACObserve(self.backView, keyPath: "bounds") ~> RAC(shadowLayer, "frame")
        backView.layer.insertSublayer(shadowLayer, atIndex: 0)
        
        let shadowLayer2 = CALayer()
        shadowLayer2.cornerRadius = 2.0
        shadowLayer2.backgroundColor = UIColor.whiteColor().CGColor
        shadowLayer2.shadowColor = UIColor.blackColor().CGColor
        shadowLayer2.shadowRadius = 2
        shadowLayer2.shadowOpacity = 0.12
        shadowLayer2.shadowOffset = CGSizeMake(1.5, 1.5)
        shadowLayer2.shouldRasterize = true
        RACObserve(self.backView, keyPath: "bounds") ~> RAC(shadowLayer2, "frame")
        backView.layer.insertSublayer(shadowLayer2 , atIndex: 0)
    }
    
    override func bindViewModel() {
        let showProducer = NSNotificationCenter.defaultCenter()
            .rac_addObserverForName(UIKeyboardWillShowNotification, object: nil)
            .toSignalProducer()
            .map({ (next) -> NSNotification in
                return next as! NSNotification
            })
            .producer
        
        let hideProducer = NSNotificationCenter.defaultCenter()
            .rac_addObserverForName(UIKeyboardWillHideNotification, object: nil)
            .toSignalProducer()
            .map({ (next) -> NSNotification in
                return next as! NSNotification
            })
            .producer
        
        let (signal, observer) = SignalProducer<SignalProducer<NSNotification, NSError>, NSError>.buffer(5)
        
        observer.sendNext(showProducer)
        observer.sendNext(hideProducer)
        
        signal.flatten(.Merge)
            .on(next: { [weak self] notification in
                guard let self_ = self else { return }
                guard let kbSizeValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue else { return }
                guard let kbDurationNumber = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber else { return }
                print(kbSizeValue)
                print(kbDurationNumber)
                self_.animateConstraint(kbSizeValue.CGRectValue().origin.y, duration: kbDurationNumber.doubleValue)
            })
            .start()

        RACObserve(self.goButton, keyPath: "enabled") ~> RAC(activityIndicator, "hidden")

        textField.rac_textSignal()
            .toSignalProducer()
            .map({ (any) -> String in
                return any as! String
            }).startWithResult({ [weak self] (result) in
                guard let self_ = self else { return }
                guard let text = result.value else { return }
                self_.findButton.enabled = text != ""
                self_.findButton.backgroundColor = text == "" ? UIColor.lightGrayColor() : UIColor.MyColorGreen()
                self_.bindUserReact([])
            })
        
        if NSUserDefaults.standardUserDefaults().objectForKey(tokenKey) != nil {
            textField.enabled = true
            loginButton.enabled = false
        }
    }
    
    override func bindActions() {
        loginButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside)
            .toSignalProducer()
            .startWithResult({ [weak self] (result) in
                guard let self_ = self else { return }
                self_.loginReact()
            })

        findButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside)
            .toSignalProducer()
            .startWithResult({ [weak self] (result) in
                guard let self_ = self else { return }
                self_.findReact()
            })
    }
    
    override func fillPage() {
        title = "Каталог"
    }
    
    func animateConstraint(origin: CGFloat, duration: Double) {
        findButtonBottomConstraint.constant = (view.frame.height - origin)
        finishConstraintAnimation(duration)
    }

    func finishConstraintAnimation(duration: Double) {
        UIView.animateWithDuration(duration) {
            self.view.layoutIfNeeded()
        }
    }
    
    func loginReact() {
        webView.hidden = false
        webView.delegate = self
        let url = NSURL(string: "https://www.instagram.com/oauth/authorize/?client_id=e92e0c34fb8145fc82af3e368cf2325c&redirect_uri=http://google.com&response_type=token&scope=public_content")
        let request = NSURLRequest(URL: url!, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 15.0)
        webView.loadRequest(request)
    }
    
    func findReact() {
        goButton.enabled = false
        viewModel.viewModelService.dataProvider.findUser(textField.text!)
            .startWithResult { [weak self] (result) in
                guard let self_ = self else { return }
                guard let users = result.value else { return }
                self_.bindUserReact(users)
        }
    }
    
    func bindUserReact(users: [UserEntity]) {
        goButton.enabled = true
        var userFound: UserEntity? = nil
        for user in users {
            if user.username!.lowercaseString == textField.text!.lowercaseString { userFound = user; break }
        }
        
        goButton.setImage(UIImage(named: userFound != nil ? "green_check" : "red_check"), forState: UIControlState.Normal)
        goButton.rac_command = RACCommand(signalBlock: {
            [weak self]
            (next) -> RACSignal! in
            guard let self_ = self else { return RACSignal.empty() }
            if userFound != nil { self_.findPhotos(userFound!) }
            return RACSignal.empty()
        })
    }
    
    func findPhotos(user: UserEntity) {
        goButton.enabled = false
        findButton.enabled = false
        findButton.backgroundColor = UIColor.lightGrayColor()
        viewModel.viewModelService.dataProvider.findUserPhoto(user)
            .startWithResult { [weak self] (result) in
                guard let self_ = self else { return }
                self_.goButton.enabled = true
                self_.findButton.enabled = true
                self_.findButton.backgroundColor = UIColor.MyColorGreen()
                guard let media = result.value else { return }
                let vm = PhotoPickerViewModel(viewModelService: self_.viewModel.viewModelService, media: media)
                self_.viewModel.viewModelService.pushPicker(vm)
        }
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return allowWebViewWithRequest(request)
    }
    
    func allowWebViewWithRequest(request: NSURLRequest) -> Bool {
        let urlString = request.URL!.absoluteString
        print("urlString", urlString)
        if urlString.containsString("www.instagram.com") { return true }
        guard urlString.containsString("access_token") else { return false }
        let array = urlString.componentsSeparatedByString("access_token=")
        let token = array[1]
        viewModel.viewModelService.setToken(token)
        textField.enabled = true
        loginButton.enabled = false
        webView.hidden = true
        textField.becomeFirstResponder()
        return true
    }
}
