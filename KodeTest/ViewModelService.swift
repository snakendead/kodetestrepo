//
//  ViewModelService.swift
//  KodeTest
//
//  Created by developer2 on 28.07.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

let tokenKey = "com.test.tokenKey"

class ViewModelService: NSObject, ViewModelServiceProtocol {

    var currentNavigationController: UINavigationController?
    var dataProvider = DataProvider()
    
    func setToken(string: String) {
        NSUserDefaults.standardUserDefaults().setObject(string, forKey: tokenKey)
    }

    func setRootFindUser() {
        let vm = FindUserViewModel(viewModelService: self)
        let vc = FindUserViewController(viewModel: vm)
        currentNavigationController = UINavigationController(rootViewController: vc)
        switchRootController(currentNavigationController!)
    }
    
    func pushPicker(viewModel: PhotoPickerViewModel) {
        let vc = PhotoPickerViewController(viewModel: viewModel)
        currentNavigationController?.pushViewController(vc, animated: true)
    }
    
    private func switchRootController(viewController: UIViewController) {
        let window = UIApplication.sharedApplication().keyWindow!
        UIView.transitionWithView(window,
                                  duration: 0.33,
                                  options: UIViewAnimationOptions.TransitionCrossDissolve,
                                  animations: {
                                    window.rootViewController = viewController
            }, completion: nil)        
    }

}
