//
//  BaseViewModel.swift
//  KodeTest
//
//  Created by developer2 on 28.07.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

class BaseViewModel: NSObject {
    
    var viewModelService: ViewModelServiceProtocol
    
    init (viewModelService: ViewModelServiceProtocol) {
        self.viewModelService = viewModelService
        super.init()
    }

}
