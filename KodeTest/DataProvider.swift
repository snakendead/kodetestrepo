//
//  DataProvider.swift
//  KodeTest
//
//  Created by developer2 on 28.07.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit
import ReactiveCocoa
import Alamofire
import SwiftyJSON

func += <K, V> (inout left: [K:V], right: [K:V]) {
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
}

class DataProvider: NSObject {

    let baseUrl = "https://api.instagram.com/v1"
    
    enum ObjectType: String {
        case Users = "/users"
        case Search = "/search"
        case Media = "/media"
        case Recent = "/recent"
    }
    
    enum APIError: ErrorType {
        case URLFailed
        case RequestFailed
        case RequestError
        case SerializationFailed
    }
    
    enum ActionType: String {
        case Get = "GET"
        case Post = "POST"
        case Put = "PUT"
        case Delete = "DELETE"
    }
    
    func findUser(nickname: String) -> SignalProducer<[UserEntity], NSError> {
        let url = baseUrl + ObjectType.Users.rawValue + ObjectType.Search.rawValue
        let producer = requestProducer(url,
                                       params: ["q": nickname],
                                       actionType: DataProvider.ActionType.Get)
        return producer.map({
            (next) -> [UserEntity] in
            print(#function, next)
            guard let dict = next as? [String: AnyObject] else { return [] }
            guard let data = dict["data"] as? [[String: AnyObject]] else { return [] }
            var elements = [UserEntity]()
            for element in data {
                let entity = UserEntity.MR_createEntity()!
                entity.safeSetValuesForKeysWithDictionary(element)
                elements.append(entity)
            }
            
            return elements
        })
    }

    func findUserPhoto(user: UserEntity) -> SignalProducer<[MediaEntity], NSError> {
        let url = baseUrl + ObjectType.Users.rawValue + "/" + user.id!  + ObjectType.Media.rawValue + ObjectType.Recent.rawValue
        let producer = requestProducer(url,
                                       params: [:],
                                       actionType: DataProvider.ActionType.Get)
        return producer.map({
            (next) -> [MediaEntity] in
            print(#function, next)
            guard let dict = next as? [String: AnyObject] else { return [] }
            guard let data = dict["data"] as? [[String: AnyObject]] else { return [] }
            var elements = [MediaEntity]()
            for element in data {
                let entity = MediaEntity.MR_createEntity()!
                entity.safeSetValuesForKeysWithDictionary(element)
                elements.append(entity)
            }
            
            return elements
        })
    }

    
    func requestProducer(urlString: String,
                         params: [String: AnyObject]?,
                         actionType: ActionType,
                         codes: [Int] = []) -> SignalProducer<AnyObject?, NSError> {
        
        return SignalProducer { observer, disposable in
            var realParams: [String: AnyObject] = ["access_token": NSUserDefaults.standardUserDefaults().objectForKey(tokenKey) as! String]
            if params != nil {  realParams += params! }
            var successCodes = [200, 201, 202]
            successCodes.appendContentsOf(codes)
            guard let url = NSURL(string: urlString) else {
                let error = NSError(domain: "Ошибка URL", code: 1001, userInfo: nil)
                observer.sendFailed(error)
                return
            }
            var headers = [String: String]()
            var encoding: ParameterEncoding = ParameterEncoding.URLEncodedInURL
            if actionType != ActionType.Get {
                if params != nil {
                    headers["Content-Type"] = "application/json"
                    encoding = ParameterEncoding.JSON
                }
            }
            let method = [ActionType.Get: Alamofire.Method.GET,
                ActionType.Post: Alamofire.Method.POST,
                ActionType.Put: Alamofire.Method.PUT,
                ActionType.Delete: Alamofire.Method.DELETE]
            
            let request = Alamofire.request(method[actionType]!,
                url,
                parameters: realParams,
                encoding: encoding,
                headers: headers)
            
            request
                .validate({ (request, response) -> Request.ValidationResult in
                    print("request", request)
                    print(response)
                    let status = response.statusCode
                    guard !successCodes.contains(status) else { return Request.ValidationResult.Success }
                    var errorDomain = ""
                    switch status {
                    case 0..<300:
                        return Request.ValidationResult.Success
                    case 400:
                        errorDomain = "Плохой, неверный запрос"
                    case 401:
                        errorDomain = "Не авторизован"
                    case 403:
                        errorDomain = "Протухший токен"
                    case 405:
                        errorDomain = "Метод не поддерживается"
                    case 409:
                        errorDomain = "Конфликт"
                    case 412:
                        errorDomain = "Условие ложно"
                    case 413:
                        errorDomain = "Размер запроса слишком велик"
                    case 501:
                        errorDomain = "Не реализовано"
                    case 503:
                        errorDomain = "Сервис недоступен"
                    case 300..<500:
                        errorDomain = "Неверный запрос"
                    case 500..<600:
                        errorDomain = "Ошибка сервера"
                    default:
                        errorDomain = "Неизвестная ошибка"
                    }
                    
                    return Request.ValidationResult.Failure(NSError(domain: errorDomain, code: status, userInfo: nil))
                })
                .responseString(completionHandler: { response in
                    switch response.result {
                    case .Success(let responseObject):
                        let data = responseObject.dataUsingEncoding(NSISOLatin1StringEncoding)
                        
                        var dict: AnyObject? = nil
                        
                        do {
                            dict = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                        } catch {
                            let error = NSError(domain: "Парсинг облажал", code: 0, userInfo: nil)
                            observer.sendFailed(error)
                        }
                        
                        let jsonDict = dict != nil ? JSON(dict!) : JSON(responseObject)
                        if jsonDict != nil {
                            if jsonDict["error"].dictionary != nil {
                                let error = NSError(domain: "Ошибка прислана в запросе", code: 1000, userInfo: nil)
                                observer.sendFailed(error)
                            }
                        }
                        if let token = request.response?.allHeaderFields["x-auth-token"] {
                            observer.sendNext(token)
                        } else {
                            observer.sendNext(jsonDict.dictionaryObject)
                        }
                        
                        observer.sendCompleted()
                        
                    case .Failure(let error):
                        observer.sendFailed(error)
                    }
                    
                    disposable.addDisposable(request.cancel)
                })
        }
    }
}

extension NSManagedObject {
    func toJSON() -> [String: AnyObject] {
        var dict = [String: AnyObject]()
        let attributes = self.entity.attributesByName
        
        for (key, _) in attributes {
            var value: AnyObject? = nil
            value = self.valueForKey(key)
            guard value != nil else { continue }
            
            if value is NSManagedObject {
                let value_ = (value as! NSManagedObject).toJSON()
                dict[key] = value_
            } else {
                dict[key] = value
            }
        }
        return dict
    }
    
    func safeSetValuesForKeysWithDictionary(keyedValues: [String: AnyObject]) {
        let attributes = self.entity.attributesByName
        for (key, attribute) in attributes {
            print("key", key)
            var newValue: AnyObject? = keyedValues[key]
            print("newValue", newValue)
            guard key != "descript" else { self.setValue(keyedValues["description"], forKey: key); continue }
            guard newValue != nil else { continue }
            let attributeType = attribute.attributeType
            switch attributeType {
            case .StringAttributeType:
                if newValue! is NSNumber { newValue = newValue!.stringValue }
            case .BooleanAttributeType:
                break
            case .Integer64AttributeType:
                break
            case .FloatAttributeType:
                break
            case .ObjectIDAttributeType:
                break
            default: break
            }
            guard !newValue!.isEqual(NSNull()) else { continue }
            self.setValue(newValue, forKey: key)
        }
        
        let relationships = self.entity.relationshipsByName
        for (key, relationship) in relationships {
            var newValue: AnyObject? = keyedValues[key]
            guard newValue != nil else { continue }
            guard !newValue!.isKindOfClass(NSNull) else { continue }

            print("key", key)
            print("newValue", newValue)

            if relationship.toMany {
                let map = ["users_in_photo": UserEntity.MR_createEntity()!]
                var set: Set<NSManagedObject> = []
                let array = newValue as! [[String: AnyObject]]
                for item in array {
                    let object = map[key]!
                    object.safeSetValuesForKeysWithDictionary(item)
                    set.insert(object)
                }
                newValue = set
            } else {
                let map = ["comments": CommentsEntity.MR_createEntity()!,
                           "caption": CaptionEntity.MR_createEntity()!,
                           "likes": LikesEntity.MR_createEntity()!,
                           "user": UserEntity.MR_createEntity()!,
                           "location": LocationEntity.MR_createEntity()!,
                           "from": UserEntity.MR_createEntity()!,
                           "low_resolution": ImageEntity.MR_createEntity()!,
                           "standard_resolution": ImageEntity.MR_createEntity()!,
                           "thumbnail": ImageEntity.MR_createEntity()!,
                           "images": ImagesEntity.MR_createEntity()!,
                           "videos": VideoEntity.MR_createEntity()!]

                if map[key] != nil {
                    let object = map[key]!
                    object.safeSetValuesForKeysWithDictionary(newValue as! [String: AnyObject])
                    newValue = object
                }
            }
            
            
            self.setValue(newValue, forKey: key)
            
        }
        
    }
}

