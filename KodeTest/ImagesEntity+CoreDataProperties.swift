//
//  ImagesEntity+CoreDataProperties.swift
//  KodeTest
//
//  Created by developer2 on 01.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ImagesEntity {

    @NSManaged var low_resolution: ImageEntity?
    @NSManaged var standart_resolution: ImageEntity?
    @NSManaged var thumbnail: ImageEntity?

}
