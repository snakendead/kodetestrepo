//
//  ViewModelServiceProtocol.swift
//  KodeTest
//
//  Created by developer2 on 28.07.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

protocol ViewModelServiceProtocol {
    
    var currentNavigationController: UINavigationController? { get }
    var dataProvider: DataProvider { get }
    
    func setToken(string: String)
    
    func setRootFindUser()
    func pushPicker(viewModel: PhotoPickerViewModel)
}