//
//  BaseViewController.swift
//  KodeTest
//
//  Created by developer2 on 28.07.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        firstInitialize()
        bindViewModel()
        bindActions()
    }
    
    func firstInitialize() {
        
    }
    
    func bindViewModel() {
        
    }
    
    func bindActions() {
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        fillPage()
    }
    
    func fillPage() {
        
    }

}
