//
//  Global.swift
//  KodeTest
//
//  Created by developer2 on 28.07.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

func UIColorFromRGB(rgbValue: Int) -> UIColor {
    return UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16)/255.0, green: CGFloat((rgbValue & 0x00FF00) >> 16)/255.0, blue: CGFloat((rgbValue & 0x0000FF) >> 16)/255.0, alpha: 1.0)
}

extension UIColor {
    class func MyColorGreen() -> UIColor {
        return UIColor(red: 76/255, green: 175/255, blue: 80/255, alpha: 1.0)
    }
}
