//
//  TableViewBindingHelper.swift
//  ReactiveSwiftFlickrSearch
//
//  Created by Colin Eberhardt on 15/07/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import Foundation
import ReactiveCocoa

class PhotoCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var likesCount: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    
    
}

class TableViewBindingHelper: NSObject, UITableViewDataSource, UITableViewDelegate {
    
  var delegate: UITableViewDelegate?
  
    private let tableView: UITableView
    private let templateCell: UITableViewCell
    private let selectionCommand: RACCommand?
    private var data: [AnyObject]
    private var reuseID: String!
    private var selectedCellIndex: Int?
    
    init(tableView: UITableView, sourceSignal: RACSignal, nibName: String, selectionCommand: RACCommand? = nil) {
        self.tableView = tableView
        self.data = []
        self.selectionCommand = selectionCommand
        self.reuseID = nibName
        let nib = UINib(nibName: nibName, bundle: nil)

        templateCell = nib.instantiateWithOwner(nil, options: nil)[0] as! UITableViewCell
        tableView.registerNib(nib, forCellReuseIdentifier: reuseID)
        
        super.init()
        
        sourceSignal.subscribeNext {
            [weak self]
            (d:AnyObject!) -> () in
            
            if d == nil {
                self?.data = [AnyObject]()
            } else {
                self?.data = d as! [AnyObject]
            }

            self?.tableView.reloadData()
        }
        
        tableView.alwaysBounceVertical = false
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(reuseID, forIndexPath: indexPath)

        cell.backgroundColor = UIColor.clearColor()
        cell.clipsToBounds = false
        cell.layer.masksToBounds = false

        let cellClassMap = ["PhotoCell": photoCell]

        let impl = cellClassMap[reuseID]!
        impl(&cell, indexPath: indexPath)
        
        return cell
    }
    
    func photoCell(inout cell: UITableViewCell, indexPath: NSIndexPath) {
        guard let photoCell = cell as? PhotoCell else { return }
        guard let media = data[indexPath.row] as? MediaEntity else { return }

        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        photoCell.photoImage.clipsToBounds = true
        photoCell.photoImage.contentMode = UIViewContentMode.ScaleAspectFill
        photoCell.likesCount.text = "\(media.likes!.count!)"
        photoCell.photoLabel.text = media.filter
        photoCell.dateLabel.text = dateFormatter.stringFromDate(NSDate(timeIntervalSince1970:Double(media.created_time!)!))

        guard photoCell.backView.layer.sublayers?.count == 6 else { return }
        photoCell.backView.layer.cornerRadius = 2.0
        photoCell.backView.layer.masksToBounds = false
        
        let shadowLayer = CALayer()
        shadowLayer.cornerRadius = 2.0
        shadowLayer.backgroundColor = UIColor.whiteColor().CGColor
        shadowLayer.shadowColor = UIColor.blackColor().CGColor
        shadowLayer.shadowRadius = 1
        shadowLayer.shadowOpacity = 0.24
        shadowLayer.shadowOffset = CGSizeMake(0, 1.0)
        shadowLayer.shouldRasterize = true
        shadowLayer.frame = photoCell.backView.bounds
        photoCell.backView.layer.insertSublayer(shadowLayer, atIndex: 0)
        
        let shadowLayer2 = CALayer()
        shadowLayer2.cornerRadius = 2.0
        shadowLayer2.backgroundColor = UIColor.whiteColor().CGColor
        shadowLayer2.shadowColor = UIColor.blackColor().CGColor
        shadowLayer2.shadowRadius = 2
        shadowLayer2.shadowOpacity = 0.12
        shadowLayer2.shadowOffset = CGSizeMake(1.5, 1.5)
        shadowLayer2.shouldRasterize = true
        shadowLayer2.frame = photoCell.backView.bounds
        photoCell.backView.layer.insertSublayer(shadowLayer2 , atIndex: 0)
        
        if let photoUrl = NSURL(string:media.images!.low_resolution!.url!) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                let data = NSData(contentsOfURL: photoUrl)
                if data != nil {
                    let image = UIImage(data: data!)
                    dispatch_async(dispatch_get_main_queue(), { 
                        photoCell.photoImage.image = image
                    })
                }
            })
        }
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return templateCell.frame.size.height
    }
  
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if selectionCommand != nil {
            selectionCommand!.execute(indexPath)
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if self.delegate?.respondsToSelector(#selector(UIScrollViewDelegate.scrollViewDidScroll(_:))) == true {
            self.delegate?.scrollViewDidScroll?(scrollView);
        }
    }
}
