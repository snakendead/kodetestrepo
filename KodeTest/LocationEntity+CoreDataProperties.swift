//
//  LocationEntity+CoreDataProperties.swift
//  KodeTest
//
//  Created by developer2 on 01.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension LocationEntity {

    @NSManaged var latitude: NSNumber?
    @NSManaged var longitude: NSNumber?
    @NSManaged var id: String?
    @NSManaged var street_address: String?
    @NSManaged var name: String?

}
