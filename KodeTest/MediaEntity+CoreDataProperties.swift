//
//  MediaEntity+CoreDataProperties.swift
//  KodeTest
//
//  Created by developer2 on 01.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension MediaEntity {

    @NSManaged var link: String?
    @NSManaged var created_time: String?
    @NSManaged var type: String?
    @NSManaged var filter: String?
    @NSManaged var tags: NSObject?
    @NSManaged var id: String?
    @NSManaged var comments: CommentsEntity?
    @NSManaged var caption: CaptionEntity?
    @NSManaged var likes: LikesEntity?
    @NSManaged var user: UserEntity?
    @NSManaged var images: ImagesEntity?
    @NSManaged var users_in_photo: NSSet?
    @NSManaged var location: LocationEntity?
    @NSManaged var videos: VideoEntity?

}
