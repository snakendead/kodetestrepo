//
//  CaptionEntity+CoreDataProperties.swift
//  KodeTest
//
//  Created by developer2 on 01.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension CaptionEntity {

    @NSManaged var created_time: String?
    @NSManaged var text: String?
    @NSManaged var id: String?
    @NSManaged var from: UserEntity?

}
