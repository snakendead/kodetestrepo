//
//  PhotoPickerViewController.swift
//  KodeTest
//
//  Created by developer2 on 28.07.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit
import ReactiveCocoa

class PhotoPickerViewController: BaseViewController, FBSDKSharingDelegate {

    @IBOutlet weak var tableView: UITableView!
    var bindingHelper: TableViewBindingHelper!
    
    var viewModel: PhotoPickerViewModel
    
    init(viewModel: PhotoPickerViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "PhotoPickerViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func firstInitialize() {
        
    }
    
    override func bindViewModel() {
        let observe = RACObserve(viewModel, keyPath: "media")
        let selectionCommand = RACCommand {
            [weak self]
            (next) -> RACSignal! in
            guard let self_ = self else { return RACSignal.empty() }
            guard let indexPath = next as? NSIndexPath else { return RACSignal.empty() }
            let cell = self_.tableView.cellForRowAtIndexPath(indexPath) as! PhotoCell
            let media = self_.viewModel.media[indexPath.row]
            self_.shareWithPhoto(media)
            return RACSignal.empty()
        }
        
        bindingHelper = TableViewBindingHelper(tableView: tableView, sourceSignal: observe, nibName: "PhotoCell", selectionCommand: selectionCommand)
    }
    
    override func bindActions() {
        
    }
    
    override func fillPage() {
        
    }

    func shareWithPhoto(media: MediaEntity) {
        let content = FBSDKShareLinkContent()
        content.contentURL = NSURL(string: "https://google.com")
        content.imageURL = NSURL(string:media.images!.low_resolution!.url!)
        content.hashtag = FBSDKHashtag(string: "#tag")
        content.contentDescription = "#some #tags #test"
        FBSDKShareDialog.showFromViewController(self, withContent: content, delegate: self)
        
    }
    
    func sharer(sharer: FBSDKSharing!, didCompleteWithResults results: [NSObject : AnyObject]!) {
        print("results", results)
    }
    
    func sharer(sharer: FBSDKSharing!, didFailWithError error: NSError!) {
        print("error", error)
    }
    
    func sharerDidCancel(sharer: FBSDKSharing!) {
        print("cancelled")
    }
}
